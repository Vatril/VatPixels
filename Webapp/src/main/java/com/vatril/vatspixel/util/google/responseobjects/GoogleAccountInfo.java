package com.vatril.vatspixel.util.google.responseobjects;

public class GoogleAccountInfo {
	
	private String id;
	private String name;
	private String given_name;
	private String family_name;
	private String complete_name;
	private String link;
	private String image_url;
	private String locale;
	public GoogleAccountInfo(String id, String name, String given_name, String family_name, 
			String link, String image_url, String locale) {
		this.id = id;
		this.name = name;
		this.given_name = given_name;
		this.family_name = family_name;
		this.complete_name = given_name + " " + family_name;
		this.link = link;
		this.image_url = image_url;
		this.locale = locale;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGiven_name() {
		return given_name;
	}
	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}
	public String getFamily_name() {
		return family_name;
	}
	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}
	public String getComplete_name() {
		return complete_name;
	}
	public void setComplete_name(String complete_name) {
		this.complete_name = complete_name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	

	
}
