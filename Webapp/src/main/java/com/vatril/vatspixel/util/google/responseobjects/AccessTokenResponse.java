package com.vatril.vatspixel.util.google.responseobjects;

public class AccessTokenResponse {
	
	private String access_token;
	private String id_token;
	private String refresh_token;
	
	public AccessTokenResponse(String access_token, String id_token, String refresh_token) {
		super();
		this.access_token = access_token;
		this.id_token = id_token;
		this.refresh_token = refresh_token;
	}
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getId_token() {
		return id_token;
	}
	public void setId_token(String id_token) {
		this.id_token = id_token;
	}
	public String getRefresh_token() {
		return refresh_token;
	}
	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}
	
	
	

}
