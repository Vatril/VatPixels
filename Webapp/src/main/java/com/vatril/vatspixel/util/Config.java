package com.vatril.vatspixel.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import javax.ws.rs.core.Context;

@Default
@ApplicationScoped
public class Config implements Serializable {

	private static final long serialVersionUID = -8311778567063453280L;
	private @Context ServletContext ctx;
	private String psqluser;
	private String psqlpass;
	private String googleClientId;
	private String googleClientSecret;

	public Config() throws IOException {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(ctx.getResourceAsStream("WEB-INF/token.json")));
		String jsonTokens = "";
		String line;
		while ((line = reader.readLine()) != null) {
			jsonTokens += line;
		}
		JSONObject jso = new JSONObject(jsonTokens);
		psqluser = jso.getString("psqluser");
		psqlpass = jso.getString("psqlpassword");
		googleClientId = jso.getString("google-client_id");
		googleClientSecret = jso.getString("google-secret");
	}

	public String getPsqluser() {
		return psqluser;
	}

	public String getPsqlpass() {
		return psqlpass;
	}

	public String getGoogleClientId() {
		return googleClientId;
	}

	public String getGoogleClientSecret() {
		return googleClientSecret;
	}

}
