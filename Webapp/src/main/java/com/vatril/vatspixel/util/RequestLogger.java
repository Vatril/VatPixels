package com.vatril.vatspixel.util;

import javax.servlet.http.HttpServletRequest;

public class RequestLogger {

	private long time;
	private String requestIP;
	private String host;
	private String language;
	private String useragent;
	private String id;

	public RequestLogger(HttpServletRequest request) {
		time = System.currentTimeMillis();
		requestIP = request.getRemoteAddr();
		host = request.getRemoteHost();
		language = request.getHeader("accept-language");
		useragent = request.getHeader("user-agent");
		id = request.getRequestURL().substring(request.getRequestURL().lastIndexOf("/") + 1, request.getRequestURL().length() - 4);
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getRequestIP() {
		return requestIP;
	}

	public void setRequestIP(String requestIP) {
		this.requestIP = requestIP;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
