package com.vatril.vatspixel.util.google;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

import org.json.JSONException;
import org.json.JSONObject;

import com.vatril.vatspixel.util.Config;
import com.vatril.vatspixel.util.google.responseobjects.AccessTokenResponse;

@Default
public class LoginHandler implements Serializable {

	private static final long serialVersionUID = 4496226902492621882L;
	private @Inject Config config;
	

	public AccessTokenResponse getTokenFromCode(String code) {
		if(code == null){
			return null;
		}
		try {
			String codeParam = "code=" + code;
			String clientIDParam = URLEncoder.encode("client_id", config.getGoogleClientId());
			String clientSecretParam = URLEncoder.encode("client_secret", config.getGoogleClientSecret());
			String grantTypeParam = URLEncoder.encode("grant_type", "authorization_code");
			String redirectUriParam = URLEncoder.encode("redirect_uri", "http://127.0.0.1:8080/VatPixels");
			URL u = new URL("https://www.googleapis.com/oauth2/v4/token?" + codeParam + "&" + clientIDParam + "&" + clientSecretParam
					 + "&" + grantTypeParam + "&" + redirectUriParam);
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setRequestProperty("content-type", "application/x-www-form-urlencoded");
			conn.setRequestMethod("POST");
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String res = "";
			String line;
			while ((line = reader.readLine()) != null) {
				res += line;
			}

			System.out.println(res);

			JSONObject json = new JSONObject(res);

			if (json.has("access_token") && json.has("id_token") && json.has("refresh_token")) {
				return new AccessTokenResponse(json.getString("access_token"), json.getString("id_token"), json.getString("refresh_token"));
			}

			return null;

		} catch (IOException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getTokenFromRefresh(AccessTokenResponse atr){
		return getTokenFromRefresh(atr.getRefresh_token());
	}
	
	public String getTokenFromRefresh(String refreshToken){
		if(refreshToken == null){
			return null;
		}
		try {
			String codeParam = URLEncoder.encode("refresh_token", refreshToken);
			String clientIDParam = URLEncoder.encode("client_id", config.getGoogleClientId());
			String clientSecretParam = URLEncoder.encode("client_secret", config.getGoogleClientSecret());
			String grantTypeParam = URLEncoder.encode("grant_type", "refresh_token");
			String redirectUriParam = URLEncoder.encode("redirect_uri", "");
			URL u = new URL("https://www.googleapis.com/oauth2/v4/token?" + codeParam + "&" + clientIDParam + "&" + clientSecretParam
					 + "&" + grantTypeParam + "&" + redirectUriParam);
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setRequestProperty("content-type", "application/x-www-form-urlencoded");
			conn.setRequestMethod("POST");
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String res = "";
			String line;
			while ((line = reader.readLine()) != null) {
				res += line;
			}

			System.out.println(res);

			JSONObject json = new JSONObject(res);

			if (json.has("access_token")) {
				return json.getString("access_token");
			}

			return null;

		} catch (IOException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

}
