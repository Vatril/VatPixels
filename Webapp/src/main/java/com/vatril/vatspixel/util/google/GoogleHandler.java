package com.vatril.vatspixel.util.google;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;

import org.json.JSONException;
import org.json.JSONObject;

import com.vatril.vatspixel.util.google.responseobjects.GoogleAccountInfo;

@Dependent
@Default
public class GoogleHandler implements Serializable {

	private static final long serialVersionUID = -7743504227709290053L;

	public GoogleAccountInfo getUserInfo(String token) {
		HttpURLConnection conn = null;
		try {
			URL u = new URL("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&code=" + token);
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod("GET");
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String res = "";
			String line;
			while ((line = reader.readLine()) != null) {
				res += line;
			}

			System.out.println(res);

			JSONObject json = new JSONObject(res);

			if (json.has("id") && json.has("name") && json.has("given_name") && json.has("family_name")
					&& json.has("link") && json.has("picture") && json.has("locale")) {
				return new GoogleAccountInfo(json.getString("id"), json.getString("name"),
						json.getString("given_name"), json.getString("family_name"),
						json.getString("link"), json.getString("picture"), json.getString("locale"));
			}
			return null;
		} catch (IOException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
