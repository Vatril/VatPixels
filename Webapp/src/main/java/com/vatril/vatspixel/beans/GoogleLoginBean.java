package com.vatril.vatspixel.beans;


import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.vatril.vatspixel.util.google.GoogleHandler;
import com.vatril.vatspixel.util.google.LoginHandler;
import com.vatril.vatspixel.util.google.responseobjects.AccessTokenResponse;
import com.vatril.vatspixel.util.google.responseobjects.GoogleAccountInfo;

import java.io.Serializable;


@Named("gl")
@SessionScoped
public class GoogleLoginBean implements Serializable{

	@Inject private HttpServletRequest request;
	
	private static final long serialVersionUID = -3827619449987661228L;
	private String authlink = "https://accounts.google.com/o/oauth2/v2/auth?"
			+ "client_id=853645930076-luf7lat3922r1gocjgq4gbfrd5s3ilbe.apps.googleusercontent.com&"
			+ "redirect_uri=http://127.0.0.1:8080/VatPixels&"
			+ "response_type=code&"
			+ "scope=https://mail.google.com/ https://www.googleapis.com/auth/userinfo.profile&"
			+ "access_type=offline&"
			+ "prompt=consent select_account";
	private AccessTokenResponse accresp;
	

	
	public String getLink() {
		return authlink;
	}
	
	public boolean islogedin(){
		if (accresp != null){
			return true;
		}else{
			accresp = new LoginHandler().getTokenFromCode(request.getParameter("code"));
			return accresp != null;
		}
	}
	
	public String getImageLink(){
		if(accresp != null){
			GoogleAccountInfo userInfo = new GoogleHandler().getUserInfo(new LoginHandler().getTokenFromRefresh(accresp));
			if(userInfo != null){
				return userInfo.getImage_url();
			}
		}
		return null;
	}
}
